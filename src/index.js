import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import reportWebVitals from './reportWebVitals'

//styles
import './Styles/Index/Index.css'
import './Styles/App/App.css'
import './Styles/Header/Header.css'

import './Styles/Sidebar/Sidebar.css'
import './Styles/MainHeader/MainHeader.css'
import './Styles/AccountExtra/AccountExtra.css'
import './Styles/CollectionBatch/CollectionBatch.css'
import './Styles/CollectionAccounts/CollectionAccounts.css'


ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
