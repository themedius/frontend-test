const parameters = {
    'ZONE_NAME': [
        'zonename',
        'zone',
        'zname'
    ],

    'REGION_NAME': [
        'regionname',
        'region'
    ],

    'LOAN_ACCOUNT_NUMBER': [
        'accountnumber',
        'loanaccountnumber'
    ],

    'BRAN_CODE': [
        'brancode',
        'bran',
        'batchcode'
    ],

    'AREANAME': [
        'areaname',
        'area'
    ],

    'BRANACHNAME': [
        'branachname',
        'branch',
    ],

    'DUE_AGMTNO': [
        'dueagmtno'
    ],

    'TENOR': [
        'tenor'
    ],

    'ADVEMI': [
        'advemi'
    ],

    'MOB': [
        'mob'
    ],

    'BKT': [
        'bkt'
    ],

    'EMI': [
        'emi'
    ],

    'DEMAND': [
        'demand'
    ],

    'OVERDUE': [
        'overdue'
    ],

    'TOTAL CHARGES': [

        'totalcharges'
    ],

    'TOTAL OD ( OD + CHARGES )': [
        'totalod(od+charges)',
        'totalod'
    ],

    'FUTURE_PRINC': [

        'futureprinc'
    ],

    'POS ( OD + FP )': [
        'pos',
        'pos(od+fp)'
    ],

    'ADDRESS': [
        'address',
        'address1'
    ],

    'Mobile Number': [
        'phonenumber',
        'contactnumber',
        'mobilenumber'
    ],

    'Alternate Number': [
        'altnumber',
        'altphonenumber',
        'altmobilenumber',
        'altcontactnumber',
        'alternatenumber',
        'alternatephonenumber',
        'alternatemobilenumber',
        'alternatecontactnumber'
    ],

    'FIRST_EMI_DATE': [
        'firstemidate'
    ],

    'LAST_EMI_DATE': [
        'lastemidate'
    ],

    'AMOUNT_FINANCED': [
        'amountfinanced',
        'amountfinanaced'
    ],

    'DISBURSAL_DATE': [
        'disbursaldate'
    ],

    'PRODUCTGROUP': [
        'productgroup'
    ],

    'STATUS': [
        'status'
    ],

    'PAYMENTTYPE': [
        'paymenttype',
    ],

    'CITY': [
        'city'
    ],

    'PINCODE': [
        'pincode',
        'pin'
    ],

    'LANDMARK': [
        'landmark'
    ],

    'OFC_ADDRESS1': [
        'officeaddress1',
        'offaddress1',
        'ofcaddress1'
    ],

    'OFC_ADDRESS2': [
        'officeaddress2',
        'offaddress2',
        'ofcaddress2'
    ],

    'OFC_ADDRESS3': [
        'officeaddress3',
        'offaddress3',
        'ofcaddress3'
    ],

    'OFC_PINCODE': [
        'officepincode',
        'offpincode',
        'ofcicepincode',
        'ofcpincode',
    ],

    'OFC_MOBILENO': [
        'officemobilenumber',
        'offemobilenumber',
        'ofcemobilenumber',
        'officemobilenumberno',
        'offemobilenumberno',
        'ofcemobilenumberno',
        'officemobileno',
        'offmobileno',
        'ofcmobileno',
    ],

    'MODEL': [
        'model'
    ],

    'REG_NUMBER': [
        'regno',
        'regnumber',
        'regionnumber'
    ],

    'CHASSIS_NO': [
        'chassisno',
        'chassisnumber'
    ],

    'ENG_NO': [
        'engno',
        'engnumber'
    ],

    'LAST_COLL_DATE': [
        'lastcollectiondate',
        'lastcoldate',
        'lastcolldate'
    ],

    'LTV': [
        'ltv'
    ],

    'LOAN_AMOUNT': [
        'loanamount'
    ],

    'CUST_PROFILE': [
        'customername',
        'firstname',
        'username',
        'fname',
        'cname',
        'customerprofile'
    ],

    'MORAT_TYPE': [
        'morattype',
        'morat'
    ]

}




export { parameters }