import React from 'react'
import axios from 'axios'
import readXlsxFile from 'read-excel-file'

import { FaEquals, FaNotEqual } from 'react-icons/fa'


import { useState, useEffect } from 'react'
import { parameters } from '../Util'
import { BsArrowUpDown } from 'react-icons/bs'

function UploadBatch() {
    const [ShowUplaodPages, setShowUplaodPages] = useState(false)
    const [ShowFilterPage, setShowFilterPage] = useState(false)
    const [OnImportPage, setOnImportPage] = useState(false)
    const [OnLoadPage, setOnLoadPage] = useState(true)

    const [ProgressBar, setProgressBar] = useState(20)
    const [headerStatus, setHeaderStatus] = useState([])
    const [headers, setHeaders] = useState([])
    const [FileName, setFileName] = useState('')
    //export matched columns
    const exportColumns = async (column_rename_data) => {

        try {
            const res = await axios.post('http://3.109.63.221:8000/dashboard/api/batch/', {
                headers: {
                    'Authorization': 'Token 2011a9d1f8ab194e24eece60aa0017e33feee783'
                },
                'batch_file': FileName,
                'batch_name': FileName,
                column_rename_data: column_rename_data
            })
            console.log({ res })
        } catch (error) {
            console.log({ error })
        }


    }

    //parse headers and assign them to headers and headerStatus
    const handleHeaders = async (file) => {
        setProgressBar(20)
        setOnLoadPage(false)
        setOnImportPage(true)
        setProgressBar(70)
        let _headers = []
        let _headerStatus = []
        setFileName(file.name)
        readXlsxFile(file).then((rows) => {
            _headers.push('Ignore this Field')
            rows[0].map(item => {
                return _headers.push(item.trim())
            })
            return _headers
        })
            .then((_headers) => {

                let _copyHeader = _headers.map(item => {
                    return item.replaceAll(' ', '').replaceAll('-', '').replaceAll('_', '').toLowerCase()
                })

                return _copyHeader
            })
            .then((_copyHeader) => {
                _copyHeader.map((header, index) => {
                    for (const [key, value] of Object.entries(parameters)) {
                        value.find(value_item => {
                            if (value_item === header) {
                                _headerStatus.push({ 'parameter': key, 'excel_header': _headers[index], 'excel_index': index })

                            }
                            return null
                        })
                    }

                    return null
                })
                return ([setHeaders(_headers), setHeaderStatus(_headerStatus), setProgressBar(100)])
            })
    }
    //check if headerStatus contain filed
    const fieldMatcher = (item1) => {
        let result = null
        headerStatus.forEach(item => {
            if (item.parameter === item1) {
                result = item.excel_header
                return result
            }
        })

        return result
    }

    const headersUpdateHandler = (parameter, excel_header, excel_index) => {
        const equalBtn = document.getElementById(`equalBtn_${parameter}`)
        const notEqualBtn = document.getElementById(`notEqualBtn_${parameter}`)
        const checkbox = document.getElementById(`checkbox_${parameter}`)

        if (excel_header === 'Ignore this Field') {
            let ogState = headerStatus
            const index = ogState.findIndex(item => item.parameter === parameter)
            if (index > -1) {
                ogState.splice(index, 1)
            }

            equalBtn.style.display = 'none'
            notEqualBtn.style.display = 'inline'
            checkbox.checked = false
            checkbox.labels[0].textContent = "Not Matched"

        } else {
            let ogState = headerStatus
            const index = ogState.findIndex(item => item.parameter === parameter)
            if (index > -1) {
                ogState[index].excel_header = excel_header
                ogState[index].excel_index = excel_index
            } else {
                ogState.push({ parameter, excel_header, excel_index })
            }

            equalBtn.style.display = 'inline'
            notEqualBtn.style.display = 'none'
            checkbox.checked = true
            checkbox.labels[0].textContent = "Matched"
            return setHeaderStatus(ogState)
        }
    }

    useEffect(() => {

    }, [headers, headerStatus])


    return (

        <div className="mainheader">
            <input type="checkbox" name="" id="" />
            <div className="mainheader_leftbtns">
                <button onClick={() => setShowFilterPage(true)}>
                    <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg">
                        <g fill="#000" fillRule="nonzero">
                            <path d="M19.2 2.4a2.4 2.4 0 0 0-2-2.364V0h-.8v.036a2.4 2.4 0 0 0 0 4.728V19.2h.8V4.764a2.4 2.4 0 0 0 2-2.364zM16.8 4a1.6 1.6 0 1 1 0-3.2 1.6 1.6 0 0 1 0 3.2zM10 10.036V0h-.8v10.036a2.4 2.4 0 0 0 0 4.728V19.2h.8v-4.436a2.4 2.4 0 0 0 0-4.728zM9.6 14a1.6 1.6 0 1 1 0-3.2 1.6 1.6 0 0 1 0 3.2zM2.8.036V0H2v.036a2.4 2.4 0 0 0 0 4.728V19.2h.8V4.764a2.4 2.4 0 0 0 0-4.728zM2.4 4a1.6 1.6 0 1 1 0-3.2 1.6 1.6 0 0 1 0 3.2z" />
                        </g>
                    </svg>
                    Filter</button>
                <button>
                    <BsArrowUpDown />
                    Sort</button>
            </div>
            <div className="mainheader_rightbtns">
                <button className="mainheader_rightbtns_white">
                    <svg width="16" height="17" xmlns="http://www.w3.org/2000/svg">
                        <g fill="#000" fillRule="evenodd">
                            <path d="M.202 11.702a.7.7 0 0 1 1.401 0v2.1a.7.7 0 0 0 .7.7h9.806a.7.7 0 0 0 .7-.7v-2.1a.7.7 0 0 1 1.401 0v2.1a2.1 2.1 0 0 1-2.1 2.1H2.303a2.1 2.1 0 0 1-2.102-2.1v-2.1z" />
                            <path d="M9.513 7.706a.7.7 0 0 1 .99.99l-2.801 2.8a.7.7 0 0 1-.99 0l-2.802-2.8a.7.7 0 1 1 .99-.99l2.306 2.306 2.307-2.306z" />
                            <path d="M6.506 1.2a.7.7 0 0 1 1.4 0v9.802a.7.7 0 0 1-1.4 0V1.2z" />
                        </g>
                    </svg>
                    Download Bulk Batch File
                </button>
                <button className="mainheader_rightbtns_black"
                    onClick={() => setShowUplaodPages(true)}>
                    <svg width="14" height="17" xmlns="http://www.w3.org/2000/svg">
                        <g fill="#FFF" fillRule="evenodd">
                            <path d="M.903 8.201a.7.7 0 0 1 1.4 0v5.601a.7.7 0 0 0 .701.7h8.405a.7.7 0 0 0 .7-.7v-5.6a.7.7 0 0 1 1.4 0v5.6a2.1 2.1 0 0 1-2.1 2.1H3.004a2.1 2.1 0 0 1-2.101-2.1v-5.6zM7.206 2.19 4.9 4.497a.7.7 0 0 1-.99-.99L6.71.706a.7.7 0 0 1 .99 0l2.802 2.8a.7.7 0 1 1-.99.99L7.206 2.19z" />
                            <path d="M6.506 1.2a.7.7 0 0 1 1.4 0v9.102a.7.7 0 0 1-1.4 0V1.2z" />
                        </g>
                    </svg>
                    Upload New Batch File
                </button>
            </div>


            {ShowUplaodPages &&
                <div className="collection_batch_extra">
                    <div className="collection_batch_upload_page">
                        <h2 className="collection_batch_upload_page_header">
                            Upload New Batch File
                        </h2>

                        <div className="collection_batch_upload_page_main">
                            <div>
                                <button className={OnLoadPage && 'collection_batch_upload_page_main_current'}>
                                    <span>01</span>
                                    <p> Import File</p></button>
                                <hr />
                                <button className={OnImportPage && 'collection_batch_upload_page_main_current'}>
                                    <span>02</span>
                                    <p>Map Columns</p></button>
                            </div>

                            {OnLoadPage &&
                                <>
                                    <svg width="155" height="148" xmlns="http://www.w3.org/2000/svg">
                                        <g fillRule="nonzero" fill="none">
                                            <g fill="#DDD">
                                                <path d="M88.397.904a3.2 3.2 0 0 0-2.714-.64l-83.2 19.2A3.2 3.2 0 0 0 0 22.614v108.8a3.2 3.2 0 0 0 2.713 3.16l83.2 12.801a3.2 3.2 0 0 0 3.687-3.161V3.414c.003-.977-.44-1.9-1.203-2.51z" />
                                                <path d="M150.4 134.613h-64a3.2 3.2 0 0 1 0-6.4h60.8v-108.8H86.4a3.2 3.2 0 0 1 0-6.4h64a3.2 3.2 0 0 1 3.2 3.2v115.2a3.2 3.2 0 0 1-3.2 3.2z" />
                                            </g>
                                            <g fill="#FFF">
                                                <path d="M60.8 102.613a3.2 3.2 0 0 1-2.713-1.504l-32-51.2a3.2 3.2 0 1 1 5.427-3.386l32 51.2a3.2 3.2 0 0 1-2.714 4.89z" />
                                                <path d="M28.8 102.613a3.2 3.2 0 0 1-2.707-4.896l32-51.2a3.2 3.2 0 1 1 5.427 3.386l-32 51.2a3.2 3.2 0 0 1-2.72 1.51z" />
                                            </g>
                                            <g fill="#DDD">
                                                <path d="M112 134.613a3.2 3.2 0 0 1-3.2-3.2v-115.2a3.2 3.2 0 0 1 6.4 0v115.2a3.2 3.2 0 0 1-3.2 3.2z" />
                                                <path d="M150.4 115.413h-64a3.2 3.2 0 0 1 0-6.4h64a3.2 3.2 0 0 1 0 6.4zM150.4 96.213h-64a3.2 3.2 0 0 1 0-6.4h64a3.2 3.2 0 0 1 0 6.4zM150.4 77.013h-64a3.2 3.2 0 0 1 0-6.4h64a3.2 3.2 0 0 1 0 6.4zM150.4 57.813h-64a3.2 3.2 0 0 1 0-6.4h64a3.2 3.2 0 0 1 0 6.4zM150.4 38.613h-64a3.2 3.2 0 0 1 0-6.4h64a3.2 3.2 0 0 1 0 6.4z" />
                                            </g>
                                        </g>
                                    </svg>
                                    <input type="file" name="uploadFile" id="uploadFile"
                                        onChange={(e) => {
                                            handleHeaders(e.target.files[0])
                                        }}
                                    />
                                    <label htmlFor="uploadFile">Select excel file from your computer</label>
                                </>
                            }

                            {OnImportPage &&
                                <>
                                    {
                                        ProgressBar < 100 ?
                                            <progress id="file" value={ProgressBar} max="100">{ProgressBar}</progress>
                                            :
                                            <div className='collection_batch_upload_page_main_fields'>
                                                <div>
                                                    <span>
                                                        Column label htmlFor File
                                                    </span>
                                                    <span>
                                                        Prospect Attributes
                                                    </span>


                                                </div>

                                                {Object.keys(parameters).map((item1) => {
                                                    let value = fieldMatcher(item1)
                                                    return (
                                                        <>
                                                            <div key={item1}>
                                                                <p>{item1}</p>
                                                                {value ?
                                                                    <>
                                                                        <FaEquals id={`equalBtn_${item1}`} style={{ opacity: '1' }} color={'#505050'} />
                                                                        <FaNotEqual id={`notEqualBtn_${item1}`} style={{ display: 'none' }} color={'#505050'} />
                                                                    </>
                                                                    :
                                                                    <>
                                                                        <FaEquals id={`equalBtn_${item1}`} style={{ display: 'none' }} color={'#505050'} />
                                                                        <FaNotEqual id={`notEqualBtn_${item1}`} style={{ opacity: '0.25' }} color={'#505050'} />

                                                                    </>}
                                                                <select name={item1} onChange={(e) => headersUpdateHandler(item1, e.target.value, e.target.selectedIndex)}>
                                                                    {headers.map((item2) => {
                                                                        return <option value={item2} selected={value === item2} >{item2} </option>
                                                                    })}
                                                                </select>
                                                                <input
                                                                    id={`checkbox_${item1}`}
                                                                    type="checkbox" name=""
                                                                    checked={value}
                                                                    onClick={() => { document.getElementById(`checkbox_${item1}`).checked = false }}
                                                                />
                                                                <label htmlFor={`checkbox_${item1}`}>{value ? 'Matched' : 'Not Matched'}</label>
                                                            </div>
                                                        </>
                                                    )
                                                })}

                                            </div>
                                    }
                                </>
                            }


                        </div>
                        <div className="collection_batch_upload_page_footer">
                            <button onClick={() => {
                                setShowUplaodPages(false)
                                setOnImportPage(false)
                                setOnLoadPage(true)
                                setProgressBar(20)
                                setHeaderStatus([])
                                setHeaders([])
                            }}>Cancel</button>

                            {OnImportPage &&
                                <button
                                    className="collection_batch_upload_page_footer_import"
                                    onClick={() => {
                                        let column_rename_data = {}
                                        headerStatus.map((i) => Object.assign(column_rename_data, { [i.parameter]: i.excel_header }),
                                            () => console.log('im callback')
                                        )
                                        exportColumns(column_rename_data)
                                        setShowUplaodPages(false)
                                        setOnImportPage(false)
                                        setOnLoadPage(true)
                                        setProgressBar(20)
                                        setHeaderStatus([])
                                        setHeaders([])
                                    }
                                    }>
                                    Import
                                </button>}

                        </div>

                    </div>
                </div>
            }

            {
                ShowFilterPage &&
                <div className="collection_batch_extra">
                    <div className="collection_batch_filter_page">
                        <h6>Filter</h6>
                        <div className="collection_batch_filter_page_search">
                            <input
                                type="text"
                                name="" id=""
                                placeholder="search by file name"
                            />
                        </div>
                        <div className="collection_batch_filter_number">
                            <div className="">
                                <label htmlFor="c_b_filter_number">Search by Batch Number</label>
                                <input type="number" name="" id="c_b_filter_number" />
                            </div>
                        </div>
                        <div className="collection_batch_filter_radio" id='batchFilterRadio'>
                            <div className="">
                                <input type="radio" name="batchFilterRadio" id="" value="All Batch" />
                                <label htmlFor="">All Batch</label>
                            </div>
                            <div className="">
                                <input type="radio" name="batchFilterRadio" id="" value="Active Batch" />
                                <label htmlFor="">Active Batch</label>
                            </div>
                            <div className="">
                                <input type="radio" name="batchFilterRadio" id="" value="Completed Batch" />
                                <label htmlFor="">Completed Batch</label>
                            </div>
                        </div>

                        <div className="collection_batch_filter_date">

                            <p>Search by Date</p>
                            <div className="">
                                <div className="">
                                    <input type="radio" name="filterByDate" id="filterByDate1" />
                                    <label htmlFor="filterByDate1">Date Between</label>
                                </div>
                                <div className="">
                                    <input className="collection_batch_filter_date_input" type="date" name="" id="" />
                                </div>
                                <div className="">
                                    <input className="collection_batch_filter_date_input" type="date" name="" id="" />
                                </div>
                            </div>

                            <div className="">
                                <div className="">
                                    <input type="radio" name="filterByDate" id="filterByDate2" />
                                    <label htmlFor="filterByDate1">Specific Date</label>
                                </div>
                                <input className="collection_batch_filter_date_input" type="date" name="" id="" />
                            </div>
                        </div>

                        <div className="collection_batch_filter_footer">
                            <button onClick={() => setShowFilterPage(false)}>Close</button>
                            <button onClick={() => setShowFilterPage(false)}> Apply</button>
                        </div>

                    </div>
                </div>
            }

        </div>
    )
}

export default UploadBatch
