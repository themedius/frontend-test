import React from 'react'

function AccountSms() {
    return (
        <div className="account_extra_sms">
            <div className="account_extra_date">
                <p>16 May 2021</p>
                <hr />
            </div>

            <div className="account_extra_sms_text">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias quo magnam expedita rerum consectetur recusandae sint fugiat voluptas delectus repudiandae!
            </div>

            <div className="account_extra_sms_text">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias quo magnam expedita rerum consectetur recusandae sint fugiat voluptas delectus repudiandae!
            </div>
        </div>
    )
}

export default AccountSms
