import React, { useState } from 'react'
import { BsArrowUpDown } from 'react-icons/bs'
function CollectionHeader() {

    const [ShowFilterPage, setShowFilterPage] = useState(false)
    return (
        <div className="mainheader">
            <input type="checkbox" name="" id="" />
            <div className="mainheader_leftbtns">
                <button onClick={() => setShowFilterPage(true)}>
                    <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg">
                        <g fill="#000" fillRule="nonzero">
                            <path d="M19.2 2.4a2.4 2.4 0 0 0-2-2.364V0h-.8v.036a2.4 2.4 0 0 0 0 4.728V19.2h.8V4.764a2.4 2.4 0 0 0 2-2.364zM16.8 4a1.6 1.6 0 1 1 0-3.2 1.6 1.6 0 0 1 0 3.2zM10 10.036V0h-.8v10.036a2.4 2.4 0 0 0 0 4.728V19.2h.8v-4.436a2.4 2.4 0 0 0 0-4.728zM9.6 14a1.6 1.6 0 1 1 0-3.2 1.6 1.6 0 0 1 0 3.2zM2.8.036V0H2v.036a2.4 2.4 0 0 0 0 4.728V19.2h.8V4.764a2.4 2.4 0 0 0 0-4.728zM2.4 4a1.6 1.6 0 1 1 0-3.2 1.6 1.6 0 0 1 0 3.2z" />
                        </g>
                    </svg>
                    Filter</button>
                <button>
                    <BsArrowUpDown />
                    Sort</button>
            </div>
            <div className="mainheader_rightbtns">
                <select className="mainheader_rightbtns_black" name="" id="">
                    <option value="" selected hidden disabled>
                        Select Option for Bulk Action
                    </option>
                    <option value="">Download</option>
                    <option value="">Issue Legal Notice</option>
                    <option value="">Communication</option>
                    <option value="">File FIR</option>
                </select>
            </div>

            {ShowFilterPage &&
                <div className="collection_batch_extra">
                    <div className="collection_batch_filter_page">
                        <h6>Filter</h6>
                        <div className="collection_batch_filter_page_search">
                            <input
                                type="text"
                                name="" id=""
                                placeholder="search by file name"
                            />
                        </div>
                        <div className="collection_batch_filter_number">

                            <div className="">
                                <label htmlFor="c_b_filter_number">Search by Batch Number</label>
                                <input type="number" name="" id="c_b_filter_number" />
                            </div>
                            <div className="">
                                <label htmlFor="c_b_filter_number">Product/Portfolio</label>
                                <select name="" id="">
                                    <option value="" selected disabled hidden>Select</option>
                                    <option value="">none</option>
                                </select>
                            </div>
                            <div className="">
                                <label htmlFor="c_b_filter_number">Overdue Bucket</label>
                                <select name="" id="">
                                    <option value="" selected disabled hidden>Select</option>
                                    <option value="">none</option>
                                </select>
                            </div>
                            <div className="">
                                <label htmlFor="c_b_filter_number">Last Disposition</label>
                                <select name="" id="">
                                    <option value="" selected disabled hidden>Select</option>
                                    <option value="">none</option>
                                </select>

                            </div>
                        </div>

                        <div className="collection_batch_filter_loan">
                            <div>
                                <label>Loan Account Range</label>
                                <input className="collection_batch_filter_date_input" type="date" name="" id="" />
                                <input className="collection_batch_filter_date_input" type="date" name="" id="" />
                            </div>
                            <div>
                                <label>Status</label>
                                <select name="" id="">
                                    <option value="" selected disabled hidden>Select</option>
                                    <option value="">none</option>
                                </select>
                            </div>
                        </div>

                        <div className="collection_batch_filter_date">

                            <p>Search by Date</p>
                            <div className="">
                                <div className="">
                                    <input type="radio" name="filterByDate" id="filterByDate1" />
                                    <label htmlFor="filterByDate1">Date Between</label>
                                </div>
                                <input className="collection_batch_filter_date_input" type="date" name="" id="" />
                                <input className="collection_batch_filter_date_input" type="date" name="" id="" />
                            </div>

                            <div className="">
                                <div className="">
                                    <input type="radio" name="filterByDate" id="filterByDate2" />
                                    <label htmlFor="filterByDate1">Specific Date</label>
                                </div>
                                <input type="date" name="" id="" className="collection_batch_filter_date_input" />
                            </div>
                        </div>

                        <div className="collection_batch_filter_footer">
                            <button onClick={() => setShowFilterPage(false)}>Close</button>
                            <button onClick={() => setShowFilterPage(false)}> Apply</button>
                        </div>

                    </div>
                </div>
            }



        </div>
    )
}

export default CollectionHeader
