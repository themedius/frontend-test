import React from 'react'

import recording from '../Static/Images/record-progress-bar.svg'
import recording1 from '../Static/Images/record-progress-bar1.svg'
import recording2 from '../Static/Images/record-progress-bar2.svg'

import play from '../Static/Images/play-button.svg'
import play1 from '../Static/Images/play-button1.svg'
import play2 from '../Static/Images/play-button2.svg'


function AccountCall(props) {
    return (
        <div className="account_extra_call">

            <div className="account_extra_date">
                <p>16 May 2021</p>
                <hr />
            </div>
            <div className="account_extra_call_audio">
                <img src={play} alt="play" />
                <div className="account_extra_call_audio_div">
                    <img src={recording} alt="recording" />
                </div>

            </div>
            <div className="account_extra_call_audio">
                <img src={play1} alt="play" />
                <div className="account_extra_call_audio_div">
                    <img src={recording1} alt="recording" />
                </div>
            </div>
            <div className="account_extra_date">
                <p>15 May 2021</p>
                <hr />
            </div>
            <div className="account_extra_call_audio">
                <img src={play2} alt="play" />
                <div className="account_extra_call_audio_div">
                    <img src={recording2} alt="recording" />
                </div>
            </div>

        </div>
    )
}

export default AccountCall
