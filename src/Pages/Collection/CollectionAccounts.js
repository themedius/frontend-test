import React, { useState } from 'react'
import data from '../../JSON/cAccounts'

import CollectionHeader from '../../Components/CollectionHeader'
import AccountCall from '../../Components/AccountCall'
import AccountSms from '../../Components/AccountSms'
import { IoCloseCircle } from 'react-icons/io5'

function CollectionAccounts() {
    const [AccountData, setAccountData] = useState(data)
    const [IsAccountCall, setIsAccountCall] = useState(false)
    const [IsAccountSms, setIsAccountSms] = useState(false)
    const [IsAccountMail, setIsAccountMail] = useState(false)



    return (
        <div>
            <CollectionHeader />
            {AccountData.map(item => {
                return (
                    <div className="collection_account">
                        <div className="collection_account_1">
                            <input type="checkbox" name="" id="" />
                            <div>
                                <section>
                                    <p> <b style={{ color: '#0a7aff', fontSize: '16px' }}> {item.account_name} ({item.account_id})</b></p>
                                    <p> Batch No. -  <strong>{item.batch_id}</strong></p>
                                </section>
                                <hr />
                                <section>
                                    <p>Product - <strong>{item.disposition_details.product}</strong></p>
                                    <p><strong>Rs.{item.disposition_details.amount}</strong></p>
                                    <p><strong>{item.disposition_details.duration}+ Days</strong></p>
                                </section>
                                <hr />
                                <section>
                                    <p >
                                        Current Status - <b style={item.status === 'High Risk' ? { color: '#e02020' } : item.status === 'Medium Risk' ? { color: '#fa6400' } : { color: '#2d9308' }}>{item.status}</b></p>
                                </section>
                            </div>

                        </div>

                        <div className="collection_account_2">
                            <div className="collection_account_2_table">
                                <span className="collection_account_2_table_a">
                                    <p>Last Disposition</p>
                                    <p> <b>{item.disposition_details.last_disposition.reason}</b></p>
                                </span>
                                <span className="collection_account_2_table_common collection_account_2_table_b1">Assigned Date</span>
                                <span className="collection_account_2_table_common collection_account_2_table_b2">{item.disposition_details.last_disposition.assigned_date}</span>
                                <span className="collection_account_2_table_common collection_account_2_table_c1">1st Called Date</span>
                                <span className="collection_account_2_table_common collection_account_2_table_c2">{item.disposition_details.last_disposition.first_called_date}</span>
                                <span className="collection_account_2_table_common collection_account_2_table_d1">Last Disposition</span>
                                <span className="collection_account_2_table_common collection_account_2_table_d2">{item.disposition_details.last_disposition.last_disposition_date}</span>
                            </div>
                        </div>
                        <div className="collection_account_3">
                            <label htmlFor="action">Action</label>
                            <select name="" id="action">
                                <option selected disabled hidden>Select</option>
                                <option value="">Download Uploaded Data</option>
                                <option value="">Download Calling Report</option>
                            </select>
                            <hr />
                            <label >View History</label>

                            <div className="collection_account_3_icon"
                                onClick={() => setIsAccountCall(true)}
                            >
                                <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M18.01 14.549a2.98 2.98 0 0 0-1.844.216l-2.068.893a20.342 20.342 0 0 1-3.668-2.91 19.872 19.872 0 0 1-2.88-3.72l.677-1.862c.24-.565.302-1.189.178-1.79L7.027 0 0 1.013l.029.427a23.18 23.18 0 0 0 6.657 14.88 22.853 22.853 0 0 0 15.15 6.72h.455l.749-7.315-5.03-1.176zm3.422 7.488a21.864 21.864 0 0 1-14.064-6.36A22.224 22.224 0 0 1 1.013 1.834l5.28-.764 1.142 4.517c.082.416.035.846-.134 1.234L6.48 9.12l.12.197a21.12 21.12 0 0 0 3.149 4.123 21.706 21.706 0 0 0 4.046 3.178l.216.139 2.554-1.1a1.992 1.992 0 0 1 1.243-.153l4.2.96-.576 5.573z" fill="#0A7AFF" fillRule="nonzero" />
                                </svg>

                                <p>Call</p>
                            </div>
                            <div className="collection_account_3_icon"
                                onClick={() => setIsAccountSms(true)}
                            >
                                <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M11.52 0A11.52 11.52 0 0 0 1.44 17.074L0 23.04l5.822-1.507A11.52 11.52 0 1 0 11.52 0zm0 22.08a10.56 10.56 0 0 1-5.38-1.474l-.174-.105-4.608 1.18 1.1-4.742-.101-.173a10.56 10.56 0 1 1 9.163 5.314z" fill="#0A7AFF" fillRule="nonzero" />
                                </svg>

                                <p>SMS</p>
                            </div>
                            <div className="collection_account_3_icon"
                                onClick={() => setIsAccountMail(true)}

                            >
                                <svg width="24" height="18" xmlns="http://www.w3.org/2000/svg">
                                    <g fill="#0A7AFF" fillRule="nonzero">
                                        <path d="M0 0v12.96a4.32 4.32 0 0 0 4.32 4.32h14.4a4.32 4.32 0 0 0 4.32-4.32V0H0zm22.08 12.96a3.36 3.36 0 0 1-3.36 3.36H4.32a3.36 3.36 0 0 1-3.36-3.36v-12h21.12v12z" />
                                        <path d="m20.39 3.682-.643-.71-8.227 7.42-8.18-7.411-.642.71 8.822 7.997z" />
                                    </g>
                                </svg>

                                <p>Email</p>
                            </div>
                            <div className="collection_account_3_icon">
                                <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg">
                                    <g fill="#0A7AFF" fillRule="nonzero">
                                        <path d="M11.52 0A11.52 11.52 0 0 0 1.44 17.074L0 23.04l5.822-1.507A11.52 11.52 0 1 0 11.52 0zm0 22.08a10.56 10.56 0 0 1-5.38-1.474l-.174-.105-4.608 1.18 1.1-4.742-.101-.173a10.56 10.56 0 1 1 9.163 5.314z" />
                                        <path d="M7.632 13.239a14.745 14.745 0 0 0 4.905 3.842c.717.34 1.676.743 2.744.812.067.003.13.006.196.006.717 0 1.293-.248 1.763-.758.003-.002.008-.008.011-.014.167-.202.357-.383.556-.576.136-.13.274-.265.406-.403.614-.64.614-1.452-.005-2.07l-1.731-1.732c-.294-.305-.645-.466-1.014-.466s-.723.161-1.025.463l-1.031 1.031a5.48 5.48 0 0 0-.286-.15 3.561 3.561 0 0 1-.316-.172c-.94-.596-1.792-1.374-2.607-2.373-.412-.522-.688-.96-.881-1.406.27-.245.524-.5.769-.751.086-.09.176-.179.265-.268.31-.311.478-.671.478-1.037 0-.366-.164-.726-.478-1.037l-.858-.858c-.101-.1-.196-.199-.294-.3-.19-.195-.389-.397-.585-.578C8.318 4.153 7.97 4 7.6 4c-.365 0-.717.153-1.025.446L5.498 5.524a2.213 2.213 0 0 0-.66 1.416c-.054.689.073 1.42.4 2.304.505 1.368 1.265 2.639 2.394 3.995zm-2.09-6.238c.034-.383.18-.703.457-.98l1.072-1.07c.167-.162.35-.245.53-.245.175 0 .354.083.518.25.193.179.374.366.57.565l.3.305.858.858c.178.179.27.36.27.539 0 .178-.092.36-.27.538l-.268.271c-.268.27-.518.527-.795.772l-.014.014c-.24.24-.202.467-.144.64.003.008.006.014.008.023.222.532.53 1.04 1.011 1.644.864 1.066 1.774 1.892 2.777 2.529.123.08.256.144.38.207.115.058.222.112.317.173l.031.017a.602.602 0 0 0 .28.072.61.61 0 0 0 .429-.196l1.077-1.077c.167-.167.348-.256.527-.256.219 0 .397.135.51.256l1.736 1.734c.346.346.343.72-.008 1.086-.121.13-.248.253-.383.383-.202.196-.412.397-.602.625-.332.357-.726.524-1.236.524-.049 0-.1-.003-.15-.006-.944-.06-1.823-.429-2.482-.743a14.008 14.008 0 0 1-4.669-3.657c-1.074-1.294-1.797-2.497-2.275-3.788-.296-.792-.409-1.428-.363-2.007z" />
                                    </g>
                                </svg>
                                <p>WhatsApp</p>
                            </div>
                            <div className="collection_account_3_icon">
                                <svg width="19" height="25" xmlns="http://www.w3.org/2000/svg">
                                    <path d="m16.852 19.622-3.525-1.961a1.079 1.079 0 0 1-.594-.97l-.002-.982a9.482 9.482 0 0 0 1.63-3.33c.007-.03.01-.06.01-.091v-.82H15.6a1.23 1.23 0 0 0 1.229-1.228V7.782a1.227 1.227 0 0 0-.82-1.153v-.075a6.554 6.554 0 0 0-13.107 0v.075c-.49.172-.817.634-.819 1.153v2.458c.002.519.33.98.82 1.153v.895a1.64 1.64 0 0 0 1.638 1.638h.563c.299.628.659 1.224 1.075 1.78v.944c.002.397-.215.763-.565.951l-3.652 1.995a3.686 3.686 0 0 0-1.927 3.243v1.327a.41.41 0 0 0 .41.41h18.022a.41.41 0 0 0 .41-.41v-1.245a3.673 3.673 0 0 0-2.025-3.299zm-.842-11.84v2.458a.41.41 0 0 1-.41.41h-1.229V7.373H15.6a.41.41 0 0 1 .41.41zM3.312 10.65a.41.41 0 0 1-.41-.41V7.782a.41.41 0 0 1 .41-.41h1.229v3.278H3.312zm1.229 2.457a.82.82 0 0 1-.82-.819v-.82h.82v.82c0 .03.003.062.01.092.059.246.131.489.217.727H4.54zm-.002-6.553h-.817A5.74 5.74 0 0 1 9.456.819a5.74 5.74 0 0 1 5.734 5.735h-.817a3.664 3.664 0 0 0-.858-2.65c-.841-.96-2.207-1.446-4.059-1.446-1.852 0-3.218.486-4.059 1.446a3.664 3.664 0 0 0-.858 2.65zM.854 23.757v-.918a2.868 2.868 0 0 1 1.5-2.524l3.65-1.993c.615-.332.996-.974.994-1.672v-.944a.808.808 0 0 0-.166-.494 9.738 9.738 0 0 1-.813-1.286h1.874c.173.49.634.818 1.153.82h1.23a1.229 1.229 0 0 0 0-2.458h-1.23c-.519.002-.98.33-1.153.82H5.641a6.369 6.369 0 0 1-.281-.867V6.554a2.842 2.842 0 0 1 .653-2.11c.679-.775 1.837-1.167 3.443-1.167 1.6 0 2.756.39 3.436 1.158a2.88 2.88 0 0 1 .66 2.119v5.687a8.666 8.666 0 0 1-1.472 2.97.808.808 0 0 0-.166.495v.985a1.886 1.886 0 0 0 1.03 1.693l3.525 1.962a2.866 2.866 0 0 1 1.589 2.575v.836H.854zm7.783-10.24a.41.41 0 0 1 .41-.41h1.228a.41.41 0 0 1 0 .82H9.046a.41.41 0 0 1-.41-.41z" fill="#0A7AFF" fillRule="nonzero" />
                                </svg>
                                <p>Dispositions</p>
                            </div>
                        </div>
                    </div>
                )
            })}

            <div className="account_extra" style={IsAccountCall ? {
                transform: 'translateX(0)', boxShadow: '0 2px 11px 7px rgba(0, 0, 0, 0.21)'
            } : { transform: 'translateX(100%)', boxShadow: 'none' }}>
                <h4>
                    Call Records (3)
                    <IoCloseCircle fill="#999999" size={22} onClick={() => setIsAccountCall(false)} />
                </h4>
                <AccountCall />
            </div>

            <div className="account_extra" style={IsAccountSms ? {
                transform: 'translateX(0)', boxShadow: '0 2px 11px 7px rgba(0, 0, 0, 0.21)'
            } : { transform: 'translateX(100%)', boxShadow: 'none' }}>
                <h4>
                    Sms Records (2)
                    <IoCloseCircle fill="#999999" size={22} onClick={() => setIsAccountSms(false)} />
                </h4>
                <AccountSms />
            </div>

            <div className="account_extra" style={IsAccountMail ? {
                transform: 'translateX(0)', boxShadow: '0 2px 11px 7px rgba(0, 0, 0, 0.21)'
            } : { transform: 'translateX(100%)', boxShadow: 'none' }}>
                <h4>
                    Mail Records (2)
                    <IoCloseCircle fill="#999999" size={22} onClick={() => setIsAccountMail(false)} />
                </h4>
                <AccountSms />
            </div>

        </div>


    )
}

export default CollectionAccounts
