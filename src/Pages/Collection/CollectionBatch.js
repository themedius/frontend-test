import React, { useEffect, useCallback } from 'react'
import axios from 'axios'
import data from '../../JSON/cBatch'
import { useState } from 'react'
//Componets
import UploadBatch from '../../Components/UploadBatch'

function CollectionBatch(props) {

    const [batchData, setbatchData] = useState(data)



    const fetchCollectionBatchData = useCallback(async () => {

        try {
            const res = await axios.get('http://3.109.63.221:8000/dashboard/api/batch/?batch_id=1', {
                headers: {
                    'Authorization': 'Token 2011a9d1f8ab194e24eece60aa0017e33feee783'
                }
            })
            console.log('data', res.data.data)
            setbatchData(res.data.data)
        } catch (error) {
            console.log({ error })
        }

    }, [])

    useEffect(() => {
        fetchCollectionBatchData()
    }, [fetchCollectionBatchData])


    return (

        <div>
            {batchData && batchData.map(item => {
                return (
                    <>

                        <div key={item.batch_id} className="collection_batch">
                            <div className="collection_batch_1">
                                <input type="checkbox" name="" id="" />
                                <div>
                                    <p style={{ fontSize: '16px' }}><b> Batch No. {item.batch_id}</b></p>
                                    <p> Ref - {item.batch_name}</p>
                                    <p>{item.uploaded_date}</p>
                                </div>
                            </div>
                            <div className="collection_batch_2">
                                <div className="collection_batch_2_1">
                                    <select name="" id="">
                                        <option value="" selected>Accounts</option>
                                    </select>
                                    <select name="" id="">
                                        <option value="" selected>All Product</option>
                                    </select>
                                </div>
                                <div className="collection_batch_2_2">{item.total_amount}</div>
                                <div className="collection_batch_2_3">

                                    <div className="collection_batch_2_3_common collection_batch_2_3_connected">
                                        <div className="coloredCirle"></div>
                                        <div>
                                            <strong>{item.total_connected}%</strong>
                                            <br />
                                            <span>Connected</span></div>
                                    </div>

                                    <div className="collection_batch_2_3_common collection_batch_2_3_ptp">
                                        <div className="coloredCirle"></div>
                                        <div>
                                            <strong>
                                                {item.total_ptp}%</strong>
                                            <br />
                                            <span>PTP</span>
                                        </div>
                                    </div>

                                    <div className="collection_batch_2_3_common collection_batch_2_3_broken">
                                        <div className="coloredCirle"></div>
                                        <div>
                                            <strong>
                                                {item.total_broken_ptp}%</strong>
                                            <br />
                                            <span>Broken PTP</span></div>
                                    </div>

                                    <div className="collection_batch_2_3_common collection_batch_2_3_paid">
                                        <div className="coloredCirle"></div>
                                        <div>
                                            <strong>
                                                {item.total_paid}%</strong>
                                            <br />
                                            <span>Paid</span></div>
                                    </div>

                                </div>
                            </div>
                            <div className="collection_batch_3">
                                <label htmlFor="action">Action</label>
                                <select name="" id="action">
                                    <option selected disabled hidden>Select</option>
                                    <option value="">Download Uploaded Data</option>
                                    <option value="">Download Calling Report</option>
                                </select>
                            </div>
                        </div>

                    </>
                )
            })
            }
        </div>
    )
}

export default CollectionBatch

