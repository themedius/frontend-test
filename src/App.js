import { BrowserRouter, Switch, Route } from 'react-router-dom'


/* Pages */
import Header from './Components/Header'
import Sidebar from './Components/Sidebar'
import Login from './Pages/Login'
//collection
import CollectionBatch from './Pages/Collection/CollectionBatch'
import CollectionAccounts from './Pages/Collection/CollectionAccounts'
import CollectionDailyReport from './Pages/Collection/CollectionDailyReport'

//litigation
import LitigationAdvocates from './Pages/Litigation/LitigationAdvocates'
import LitigationBatch from './Pages/Litigation/LitigationBatch'
import LitigationCalender from './Pages/Litigation/LitigationCalender'
import LitigationCase from './Pages/Litigation/LitigationCase'

//pre-lirigation
import PreLitigationBatch from './Pages/Pre-Litigation/PreLitigationBatch'
import PreLitigationFir from './Pages/Pre-Litigation/PreLitigationFir'
import PreLitigationNotices from './Pages/Pre-Litigation/PreLitigationNotices'

//others
import Account from './Pages/Account'
import Communication from './Pages/Communication'
import Dashboard from './Pages/Dashboard'
import Invoices from './Pages/Invoices'
import Settings from './Pages/Settings'
import SuperAdmin from './Pages/SuperAdmin'
import Users from './Pages/Users'


/* Components */



function App() {

  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <main >
          <div />
          <Switch>
            <Route exact path={'/'}>
              <CollectionBatch />
            </Route>
            <Route exact path={'/login'}>
              <Login />
            </Route>
            <Route exact path={'/collection/batch'}>
              <CollectionBatch />
            </Route>
            <Route exact path={'/collection/accounts'}>
              <CollectionAccounts />
            </Route>
            <Route exact path={'/collection/dailyreport'}>
              <CollectionDailyReport />
            </Route>
            <Route exact path={'/litigation/advocates'}>
              <LitigationAdvocates />
            </Route>
            <Route exact path={'/litigation/batch'}>
              <LitigationBatch />
            </Route>
            <Route exact path={'/litigation/calender'}>
              <LitigationCalender />
            </Route>
            <Route exact path={'/litigation/case'}>
              <LitigationCase />
            </Route>
            <Route exact path={'/prelitigation/batch'}>
              <PreLitigationBatch />
            </Route>
            <Route exact path={'/prelitigation/fir'}>
              <PreLitigationFir />
            </Route>
            <Route exact path={'/prelitigation/notices'}>
              <PreLitigationNotices />
            </Route>

            <Route exact path={'/account'}>
              <Account />
            </Route>
            <Route exact path={'/communication'}>
              <Communication />
            </Route>
            <Route exact path={'/invoices'}>
              <Invoices />
            </Route>
            <Route exact path={'/settings'}>
              <Settings />
            </Route>
            <Route exact path={'/superadmin'}>
              <SuperAdmin />
            </Route>
            <Route exact path={'/users'}>
              <Users />
            </Route>
          </Switch>
        </main>
      </BrowserRouter>
    </div>
  )
}

export default App
